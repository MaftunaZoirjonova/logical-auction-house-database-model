CREATE TABLE `AuctionAntique`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Auction_ID` BIGINT NOT NULL,
    `Antique_ID` BIGINT NOT NULL,
    `Lot_Number` VARCHAR(255) NOT NULL,
    `Start_Price` BIGINT NOT NULL
);
CREATE TABLE `Buyers`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `User_ID` BIGINT NOT NULL
);
CREATE TABLE `Antique`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Product_Code` VARCHAR(255) NOT NULL,
    `Name` VARCHAR(255) NOT NULL,
    `Country` VARCHAR(255) NOT NULL,
    `Era` BIGINT NOT NULL,
    `Batch_ID` BIGINT NOT NULL
);
CREATE TABLE `Users`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Name` VARCHAR(255) NOT NULL,
    `Contact` VARCHAR(255) NOT NULL,
    `Juridical_Type` VARCHAR(255) NOT NULL
);
CREATE TABLE `SoldArtworks`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Buyer_ID` BIGINT NOT NULL,
    `Auction_ID` BIGINT NOT NULL,
    `Actual_Price` BIGINT NOT NULL,
    `AuctionArtwork_ID` BIGINT NOT NULL
);
CREATE TABLE `Auction`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Location` VARCHAR(255) NOT NULL,
    `Date` DATE NOT NULL,
    `Time` TIME NOT NULL,
    `Details` VARCHAR(255) NOT NULL
);
CREATE TABLE `AuctionArtworks`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Auction_ID` BIGINT NOT NULL,
    `Artwork_ID` BIGINT NOT NULL,
    `Lot_Number` VARCHAR(255) NOT NULL,
    `Start_Price` BIGINT NOT NULL
);
CREATE TABLE `Sellers`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `User_ID` BIGINT NOT NULL
);
CREATE TABLE `Artworks`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Product_Code` VARCHAR(255) NOT NULL,
    `Country` VARCHAR(255) NOT NULL,
    `Artist` VARCHAR(255) NOT NULL,
    `Style` VARCHAR(255) NOT NULL,
    `Year` BIGINT NOT NULL,
    `Batch_ID` BIGINT NOT NULL
);
CREATE TABLE `Batches`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Seller_ID` BIGINT NOT NULL
);
CREATE TABLE `SoldAntiques`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `Buyer_ID` BIGINT NOT NULL,
    `Actual_Price` BIGINT NOT NULL,
    `Auction_ID` BIGINT NOT NULL,
    `AuctionAntique_ID` BIGINT NOT NULL
);
ALTER TABLE
    `AuctionAntique` ADD CONSTRAINT `auctionantique_auction_id_foreign` FOREIGN KEY(`Auction_ID`) REFERENCES `Auction`(`id`);
ALTER TABLE
    `SoldArtworks` ADD CONSTRAINT `soldartworks_auctionartwork_id_foreign` FOREIGN KEY(`AuctionArtwork_ID`) REFERENCES `AuctionArtworks`(`id`);
ALTER TABLE
    `AuctionArtworks` ADD CONSTRAINT `auctionartworks_artwork_id_foreign` FOREIGN KEY(`Artwork_ID`) REFERENCES `Artworks`(`id`);
ALTER TABLE
    `SoldArtworks` ADD CONSTRAINT `soldartworks_auction_id_foreign` FOREIGN KEY(`Auction_ID`) REFERENCES `Auction`(`id`);
ALTER TABLE
    `Users` ADD CONSTRAINT `users_id_foreign` FOREIGN KEY(`id`) REFERENCES `Buyers`(`id`);
ALTER TABLE
    `SoldAntiques` ADD CONSTRAINT `soldantiques_auction_id_foreign` FOREIGN KEY(`Auction_ID`) REFERENCES `Auction`(`id`);
ALTER TABLE
    `SoldAntiques` ADD CONSTRAINT `soldantiques_buyer_id_foreign` FOREIGN KEY(`Buyer_ID`) REFERENCES `Buyers`(`id`);
ALTER TABLE
    `Antique` ADD CONSTRAINT `antique_batch_id_foreign` FOREIGN KEY(`Batch_ID`) REFERENCES `Batches`(`id`);
ALTER TABLE
    `Batches` ADD CONSTRAINT `batches_seller_id_foreign` FOREIGN KEY(`Seller_ID`) REFERENCES `Sellers`(`id`);
ALTER TABLE
    `Sellers` ADD CONSTRAINT `sellers_user_id_foreign` FOREIGN KEY(`User_ID`) REFERENCES `Users`(`id`);
ALTER TABLE
    `SoldAntiques` ADD CONSTRAINT `soldantiques_auctionantique_id_foreign` FOREIGN KEY(`AuctionAntique_ID`) REFERENCES `AuctionAntique`(`id`);
ALTER TABLE
    `SoldArtworks` ADD CONSTRAINT `soldartworks_buyer_id_foreign` FOREIGN KEY(`Buyer_ID`) REFERENCES `Buyers`(`id`);
ALTER TABLE
    `AuctionAntique` ADD CONSTRAINT `auctionantique_antique_id_foreign` FOREIGN KEY(`Antique_ID`) REFERENCES `Antique`(`id`);
ALTER TABLE
    `AuctionArtworks` ADD CONSTRAINT `auctionartworks_auction_id_foreign` FOREIGN KEY(`Auction_ID`) REFERENCES `Auction`(`id`);
ALTER TABLE
    `Artworks` ADD CONSTRAINT `artworks_batch_id_foreign` FOREIGN KEY(`Batch_ID`) REFERENCES `Batches`(`id`);